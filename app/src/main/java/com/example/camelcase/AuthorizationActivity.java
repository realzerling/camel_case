package com.example.camelcase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.EditText;
import android.widget.Toast;

import com.example.camelcase.lanclass.AuthResponse;
import com.example.camelcase.lanclass.RetroFitApiBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthorizationActivity extends AppCompatActivity {
    EditText log,pass;
    RetriInterface retriInterface;//lan
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        retriInterface= RetroFitApiBuilder.getRetriInterface();

    }

    public void sigin(View view) {

        Intent intent=new Intent( AuthorizationActivity.this, RegistrationActivity.class);
        startActivity(intent);
       //setContentView(R.layout.activity_registration);
    }

    public void loginbuttonclick(View view) {
         log = (EditText) findViewById(R.id.logintextbox);
        pass = (EditText) findViewById(R.id.passwordeditbox);
            final String login=log.getText().toString();
            final String password=pass.getText().toString();
            if (!login.isEmpty()&&!password.isEmpty()){
                Call <AuthResponse> authCall = retriInterface.authUser(login,password);
                authCall.enqueue(new Callback<AuthResponse>() {
                    @Override
                    public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                          if (response.isSuccessful()){

                    //    if response.body()!=null {//if не пришла пустота, то бишь юзер есть такой на сервере то переход к меню
                            Intent intent=new Intent( AuthorizationActivity.this,MenuActivity.class);
                            startActivity(intent);}
                          else{
                              Toast.makeText(getApplicationContext(),"Неверный логин или пароль",Toast.LENGTH_LONG).show();
                          }

                    }

                    @Override
                    public void onFailure(Call<AuthResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),"Ошибка сервера",Toast.LENGTH_LONG).show();
                    }
                });


               // Intent intent=new Intent( AuthorizationActivity.this,MenuActivity.class);
                //startActivity(intent);
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Пустые поля не допускаются",Toast.LENGTH_SHORT).show();
            }
    }
}