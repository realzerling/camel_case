package com.example.camelcase.lanclass;

import java.util.ArrayList;

public class PizzaList {
    ArrayList<PizzaListItem>  pizza;

    public ArrayList<PizzaListItem> getPizza() {
        return pizza;
    }

    public void setPizza(ArrayList<PizzaListItem> pizza) {
        this.pizza = pizza;
    }

    public PizzaList(ArrayList<PizzaListItem> pizza) {
        this.pizza = pizza;
    }
}
