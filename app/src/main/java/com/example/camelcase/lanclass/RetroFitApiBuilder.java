package com.example.camelcase.lanclass;

import com.example.camelcase.RetriInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroFitApiBuilder {
    static  public RetriInterface getRetriInterface(){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("http://eetk.tesan.ru:8089")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetriInterface retriInterface=retrofit.create(RetriInterface.class);
        return   retriInterface ;
    }
}
