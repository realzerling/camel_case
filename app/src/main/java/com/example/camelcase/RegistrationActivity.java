package com.example.camelcase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.camelcase.lanclass.RetroFitApiBuilder;
import com.example.camelcase.lanclass.UserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegistrationActivity extends AppCompatActivity
{
    SharedPreferences sharedPreferences;//надо для сохранения инфы в телефоне
    SharedPreferences.Editor editor;//надо для сохранения инфы в телефоне
    //Retrofit retrofit;//надо для работы по сети
    RetriInterface retriInterface;//надо для работы по сети
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        EditText q = (EditText) findViewById(R.id.telefonedittext);
        q.addTextChangedListener(new PhoneTextFormatter(q, "+7(9##)-###-##-##"));//из инета скачал класс для маски ввода телефонного номера
        q.setText("+7(9");//tel
        q.setSelection(4);//tel
        //дальше настраиваем ретрофит чтоб с сетью работать
        RetroFitApiBuilder retroFitApiBuilder=new RetroFitApiBuilder();
        retriInterface = retroFitApiBuilder.getRetriInterface();

    }

    public void cancelbuttonclick(View view) {
            Intent intent=new Intent( RegistrationActivity.this,AuthorizationActivity.class);
            startActivity(intent);
    }

    public void regbuttonclick(View view)
    {
        EditText log = (EditText) findViewById(R.id.logintextbox);
        EditText pass = (EditText) findViewById(R.id.passwordeditbox);
        EditText reppass = (EditText) findViewById(R.id.repeatpasswordeditbox);
        EditText pochta= (EditText) findViewById(R.id.emailadresbox);
        final String login=log.getText().toString();
        final String password=pass.getText().toString();
        final String reppassword=reppass.getText().toString();
        final String epochta;
        epochta = pochta.getText().toString();

        if ((password!=reppassword)&&(epochta.contains("@")))
             {
                 Call<UserResponse> responseCall = retriInterface.regUser(login,epochta, password); //Засылаем запрос на сервер на проверку регистрации юзера
                 responseCall.enqueue(new Callback<UserResponse>() {
                     @Override
                     public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        Toast.makeText(getApplication(),response.body().getLogin(),Toast.LENGTH_SHORT).show();

                        //Intent intent=new Intent( RegistrationActivity.this,MenuActivity.class);
                        Intent intent=new Intent( RegistrationActivity.this,AuthorizationActivity.class);
                        startActivity(intent);
                     }

                     @Override
                     public void onFailure(Call<UserResponse> call, Throwable t) {

                     }
                 });


            }
            else
            {
                Toast.makeText(getApplicationContext(),"Пароли не совпадают или почта  не указана корректно",Toast.LENGTH_SHORT).show();
            }
    }

    public void saveButtonClick(View view) {
        EditText log = (EditText) findViewById(R.id.logintextbox);
        EditText tel = (EditText) findViewById(R.id.telefonedittext);
        EditText nameklienta = (EditText) findViewById(R.id.nametextbox);
        EditText pochta= (EditText) findViewById(R.id.emailadresbox);
        ImageButton loadbutton=(ImageButton) findViewById(R.id.loadLocalInfoButton);
        final String login=log.getText().toString();
        final String telefon=tel.getText().toString();
        final String name=nameklienta.getText().toString();
        final String epochta=pochta.getText().toString();

        if (!(login.isEmpty()&&telefon.isEmpty()&&name.isEmpty()&&epochta.isEmpty()))
        {
            SharedPreferences sharedPreferences=getSharedPreferences("main",MODE_PRIVATE);
            editor =sharedPreferences.edit();
            editor.putString("name", name).putString("telefon", telefon).putString("login", login).putString("pochta", epochta);
            editor.apply();
           // loadbutton.setEnabled(true);
            Toast.makeText(getApplicationContext(),"Информация записана в телефон",Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Чтото не введено",Toast.LENGTH_SHORT).show();
        }

    }//savebuttonclick

    public void loadlocalinfobuttonclick(View view) {

        EditText log = (EditText) findViewById(R.id.logintextbox);
        EditText tel = (EditText) findViewById(R.id.telefonedittext);
        EditText nameklienta = (EditText) findViewById(R.id.nametextbox);
        EditText pochta= (EditText) findViewById(R.id.emailadresbox);
        ImageButton loadbutton=(ImageButton) findViewById(R.id.loadLocalInfoButton);
        final String login=log.getText().toString();
        final String telefon=tel.getText().toString();
        final String name=nameklienta.getText().toString();
        final String epochta=pochta.getText().toString();

        SharedPreferences sharedPreferences=getSharedPreferences("main",MODE_PRIVATE);
        nameklienta.setText(sharedPreferences.getString("name","petja"));
        tel.setText(sharedPreferences.getString("telefon","+9(222)100-66-11"));
        log.setText(sharedPreferences.getString("login","admi"));
        pochta.setText(sharedPreferences.getString("pochta","petja@sasa.com"));
        Toast.makeText(getApplicationContext(),"Информация считана",Toast.LENGTH_SHORT).show();
    }
}//class
