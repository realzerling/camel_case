package com.example.camelcase;

import android.content.Intent;

import com.example.camelcase.lanclass.AuthResponse;
import com.example.camelcase.lanclass.PizzaList;
import com.example.camelcase.lanclass.PizzaListItem;
import com.example.camelcase.lanclass.UserResponse;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.Call;
import retrofit2.http.Query;

public interface RetriInterface {
    @POST("/pizza/reg")
    Call<UserResponse> regUser(
            @Query("login") String login,
            @Query("email") String email,
            @Query("password") String password
    );
    @GET ("/pizza")
    Call<PizzaList> getPizzalist();
    @POST("/pizza")
    Call<PizzaListItem> getPizza(
            @Query("id")Intent id
            );
    @POST("/pizza/auth")
    Call<AuthResponse> authUser (
            @Query("login") String login,
            @Query("password") String password
    );
}
