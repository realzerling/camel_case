package com.example.camelcase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class PizzaActivity extends AppCompatActivity {
    TextView  textView;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza);
        textView=findViewById(R.id.pizzatitle);
        imageView=findViewById(R.id.pizzaimage);
        //String q;
        //q=getIntent().getStringExtra("title");//получаем инфу с того эрана из сообщения
        //textView.setText(q);
        textView.setText(getIntent().getStringExtra("title"));
        imageView.setImageResource(getIntent().getIntExtra("image",R.drawable.ic_home_black_24dp));

    }
}