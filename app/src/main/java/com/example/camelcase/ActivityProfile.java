package com.example.camelcase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActivityProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }

    public void emailchangebuttonclick(View view) {
    }

    public void passwordchangebuttonclick(View view) {
    }

    public void exitbuttonclick(View view) {
        Intent intent=new Intent(ActivityProfile.this,AuthorizationActivity.class);
        startActivity(intent);
        Button button;//для анимации
        AnimationDrawable ad;//для анимации
        button=findViewById(R.id.exitbutton);
        ad= (AnimationDrawable) button.getBackground();
        ad.setEnterFadeDuration(100);
        ad.setExitFadeDuration(3000);
        ad.start();
    }
}