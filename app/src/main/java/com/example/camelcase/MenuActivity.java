package com.example.camelcase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MenuActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    String titles[],descriptions[];
    int images[]={R.drawable.pizza,R.drawable.classic,R.drawable.margarita,
            R.drawable.meat,R.drawable.mushrooms,R.drawable.olives,R.drawable.pepperoni,
            R.drawable.vegetarian,R.drawable.cheese,R.drawable.ham};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        recyclerView=findViewById(R.id.pizzalist);
        titles=getResources().getStringArray(R.array.titlepizza);
        descriptions=getResources().getStringArray(R.array.decriptionpizza);
        MyRecycleViewAdapter recycleViewAdapter = new MyRecycleViewAdapter(titles,descriptions,images,this);
        recyclerView.setAdapter(recycleViewAdapter);
        //images=getResources().getIntArray(R.array.)
    }

    public void profileClick(View view) {
        Intent intent = new Intent(MenuActivity.this,ActivityProfile.class); //надо сделать профиля активити
        startActivity(intent);
    }
}