package com.example.camelcase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyRecycleViewAdapter extends RecyclerView.Adapter<MyRecycleViewAdapter.MyViewHolder> {
    String titles[],descriptions[];
    int images[];
    Context context;

    public MyRecycleViewAdapter(String[] titles, String[] descriptions, int[] images, Context context) {
        this.titles = titles;
        this.descriptions = descriptions;
        this.images = images;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View root =inflater.inflate(R.layout.row_element, parent,false);
        return new MyViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.title.setText(titles[position]);
        holder.desc.setText(descriptions[position]);
        holder.imagepizza.setImageResource(images[position]);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    //Toast.makeText(context,titles[position],Toast.LENGTH_SHORT);
                //Intent intent=new Intent(MenuActivity.this,PizzaActivity.class);
                Intent intent=new Intent(context,PizzaActivity.class);
                intent.putExtra("title",titles[position]);//передаем инфу на тот экран типа сообщения
                intent.putExtra("Image",images[position]);
                context.startActivity(intent);

            }
        });


        /*holder.imagepizza.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"pict"+titles[position],Toast.LENGTH_SHORT);
            }
        });*/
    }

    @Override
    public int getItemCount() {

        return images.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title,desc;
        ImageView imagepizza;
        LinearLayout linearLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.titleitem);
            desc = itemView.findViewById(R.id.decriptionitem);
            imagepizza=itemView.findViewById(R.id.pizzaitem);
            linearLayout=itemView.findViewById(R.id.linearpizza);
        }
    }
}
