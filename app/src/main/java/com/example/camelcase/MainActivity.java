package com.example.camelcase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                ConstraintLayout ll;//для анимации
                AnimationDrawable ad;//для анимации
                ll=findViewById(R.id.mainlinearLayout);
                ad= (AnimationDrawable) ll.getBackground();
                ad.setEnterFadeDuration(10);
                ad.setExitFadeDuration(1000);
                ad.start();

            }

            @Override
            public void onFinish() {
                Intent intent =new Intent( MainActivity.this,AuthorizationActivity.class);
                startActivity(intent);

            }
        }.start();


    }
}